﻿// ------------------------------------------------------------------------------------------------
//  <copyright file="MainWindow.xaml.cs"
//             company="gtaroleplay.de / fluxter.net">
//       Copyright (c) gtaroleplay.de / fluxter.net. All rights reserved.
//  </copyright>
//  <author>Marcel Kallen</author>
//  <created>04.03.2018 - 19:50</created>
// ------------------------------------------------------------------------------------------------

namespace E621_PoolDownloader
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Forms;
    using Core;
    using Models;

    /// <summary>
    ///     Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private string BaseDirectory { get; set; }

        private WebClient WebClient
        {
            get
            {
                var wc = new WebClient();
                wc.Headers.Add("user-agent", "E621 PoolDownloader/1.0 (by vito on e621)");
                return wc;

            }
        }

        private string SelectDirectory()
        {
            if (this.BaseDirectory == null)
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    var result = fbd.ShowDialog();

                    if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        BaseDirectory = fbd.SelectedPath;
                    }
                }
            }

            return this.BaseDirectory;
        }

        private void DownloadPoolsByTag_OnClick(object sender, RoutedEventArgs e)
        {
        }

        private int FoundPools = 0;

        private int DownloadedPools = 0;

        private async void DownloadPoolListButton_OnClick(object sender, RoutedEventArgs e)
        {
            var directory = this.SelectDirectory();
            var api = new E621Api();
            System.Windows.Application.Current.Dispatcher.Invoke(() => { this.DownloadPoolListButton.IsEnabled = false; });

            var tasks = new List<Task>();
            var tags = this.DownloadPoolListUrl.Text;
            await Task.Run(() =>
            {
                foreach (var pool in api.GetPoolsByTag(tags))
                {
                    this.FoundPools++;
                    System.Windows.Application.Current.Dispatcher.Invoke(() =>
                    {
                        this.DownloadPoolListCount.Content = "Gefundene Pools: " + this.FoundPools;
                    });

                    var dirname = pool.Name;
                    var invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                    foreach (var c in invalid)
                    {
                        dirname = dirname.Replace(c.ToString(), "");
                    }

                    var targetDir = Path.Combine(directory, dirname);

                    var task = Task.Run(() => api.DownloadPostsByPoolIdAsync(pool.Id, targetDir));
                    tasks.Add(task);
                }
            });

            await Task.WhenAll(tasks);
            System.Windows.Application.Current.Dispatcher.Invoke(() => { this.DownloadPoolListButton.IsEnabled = true; });
        }

        private async void DownloadPoolButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.DownloadPoolProgress.Value = 0;
            var url = this.DownloadPoolUrl.Text;
            var regex = new Regex(@"\/pool\/show\/(\d+)");
            if (!regex.IsMatch(url))
            {
                return;
            }

            var id = Convert.ToInt32(regex.Match(url).Groups[1].Value);

            var directory = this.SelectDirectory();
            var api = new E621Api();
            System.Windows.Application.Current.Dispatcher.Invoke(() => { this.DownloadPoolButton.IsEnabled = false; });
            var pool = new Pool(api, id);
            var targetDir = Path.Combine(directory, pool.Name.Replace(":", ""));
            await Task.Run(() => api.DownloadPostsByPoolIdAsync(id, targetDir, progress =>
            {
                System.Windows.Application.Current.Dispatcher.Invoke(() => { this.DownloadPoolProgress.Value = progress; });
            }));
            System.Windows.Application.Current.Dispatcher.Invoke(() => { this.DownloadPoolButton.IsEnabled = true; });
        }
    }
}